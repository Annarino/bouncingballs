import {generateRandomNumber, generateRandomSign} from './UtilityTools.js'
import { Ball } from './Ball.js';

export class StaticBalls {
    constructor(canvas, options = {}){
        this.canvas = canvas;
        this.context = canvas.getContext('2d');
        this.numberOfBalls = options.numberOfBalls || 50;
        this.balls = new Array(this.numberOfBalls);
        this.maxRadius = options.maxRadius || 20;
        this.minRadius = options.minRadius || 5
        this.fillColorPallet = options.fillColorPallet || ['#41E8B0', '#64AC94', '#2C9C76', '#00E69A', '#1E6B51'];
        this.strokeColorPallet = options.strokeColorPallet || ['#1f1f1f'];

        this.instantiateBalls();
    }

    instantiateBalls(){
        for(let i = 0; i < this.balls.length; i++){
            const ball = new Ball();
            ball.setRadius(generateRandomNumber(this.minRadius, this.maxRadius, true))
            ball.setXCoordinate(generateRandomNumber(ball.getRadius(), this.canvas.width - ball.getRadius(), true));
            ball.setYCoordinate(generateRandomNumber(ball.getRadius(), this.canvas.height - ball.getRadius(), true));
            ball.setStrokeColor(this.strokeColorPallet[generateRandomNumber(0, this.strokeColorPallet.length, true)]);
            ball.setColor(this.fillColorPallet[generateRandomNumber(0, this.fillColorPallet.length, true)]);
            this.balls[i] = ball;
        }  
    }

    drawBalls(){
        for(let ball of this.balls){
            this.context.beginPath();
            this.context.arc(ball.getXCoordinate(), ball.getYCoordinate(), ball.getRadius(), 0, 2*Math.PI);
            this.context.fillStyle = ball.getColor();
            this.context.strokeStyle = ball.getStrokeColor();
            this.context.stroke();
            this.context.fill();
            this.context.closePath();
        }  
    }

    setColorPallet(colors){
        this.fillColorPallet = [...colors];
    }

    changeBallColors(){
        for(let ball of this.balls){
            ball.setColor(this.fillColorPallet[generateRandomNumber(0, this.fillColorPallet.length, true)]);
        }
        this.drawBalls();
    }

    resetBalls(){
        this.context.clearRect(0,0, this.canvas.width, this.canvas.height);
        this.instantiateBalls();
        this.drawBalls();
    }
}