import {gravity} from './constants.js';
import {generateRandomNumber} from './UtilityTools.js'
import {Ball} from './Ball.js';

export class GravityBalls {
    constructor(canvas, options = {}){
        this.canvas = canvas;
        this.context = this.canvas.getContext('2d');
        this.animate = true;
        this.bounce = false;
        this.numberOfBalls = options.numberOfBalls || 50;
        this.yMaxVelocity = options.yMaxVelocity || 100;
        this.yMinVelocity = options.yMinVelocity || 50;
        this.maxRadius = options.maxRadius || 20;
        this.minRadius = options.minRadius || 5
        this.balls = new Array(this.numberOfBalls);
        this.fillColorPallet = options.fillColorPallet || ['#41E8B0', '#64AC94', '#2C9C76', '#00E69A', '#1E6B51'];
        this.strokeColorPallet = options.strokeColorPallet || ['#1f1f1f'];
        this.createBalls();
    }

    resetBallsWithNewAttributes(){
        this.createBalls();
    }

    createBalls(){
        for(let i = 0; i < this.balls.length; i++){
            let ball = {}
                ball.color = this.fillColorPallet[generateRandomNumber(0, this.fillColorPallet.length, true)];
                ball.strokeColor = this.strokeColorPallet[generateRandomNumber(0, this.strokeColorPallet.length, true)];;
                ball.radius = generateRandomNumber(this.minRadius, this.maxRadius, true);
                ball.xCoordinate = generateRandomNumber(ball.radius, this.canvas.width - ball.radius, true);
                ball.yCoordinate = generateRandomNumber(this.canvas.height - ball.radius, 0, true); 
                ball.mass = generateRandomNumber(1, 100);
                ball.startingVelocity = -generateRandomNumber(this.yMinVelocity, this.yMaxVelocity)
                ball.yVelocity = ball.startingVelocity;
                ball.xVelocity = 0;
                ball.velocityLossToFriction = ball.startingVelocity;
                ball.frictionCoefficient = .9;
                ball.minimumVelocity = -1;
                ball.timeInterval = 0.3;
            ball = new Ball(ball);
            this.balls[i] = ball;
        }
    }

    animateBalls(){
        let animationFrame;
        if(this.animate){
             animationFrame = requestAnimationFrame(() => this.animateBalls());
        } else {
            cancelAnimationFrame(animationFrame);
            this.context.clearRect(0,0,this.canvas.width, this.canvas.height)
        }

        this.context.clearRect(0,0, innerWidth, innerHeight);
        this.animate = false;

        for(let ball of this.balls){
            if(ball.velocityLossToFriction < ball.minimumVelocity){
                this.animate = true;
            }
            this.drawBall(ball);
            this.updateYCoordinate(ball);
        }
    }

    drawBall(ball){
        this.context.beginPath();
        this.context.arc(ball.getXCoordinate(), ball.getYCoordinate(), ball.getRadius(), 0, 2*Math.PI, false);
        this.context.strokeStyle = ball.getStrokeColor();
        this.context.fillStyle = ball.getColor();
        this.context.stroke();
        this.context.fill();
    }

    updateYCoordinate(ball){
        ball.setYCoordinate(ball.getYCoordinate() + (ball.getYVelocity() * ball.getTimeInterval()) + (0.5 * gravity * ball.getTimeInterval()**2));
        this.updateVelocity(ball);
    }

    updateVelocity(ball){
        let updatedVelocity = ball.getYVelocity() + gravity * ball.getTimeInterval();
        if(ball.getYCoordinate() > this.canvas.height - ball.getRadius()){
            ball.setYCoordinate(this.canvas.height - ball.getRadius());
            ball.setVelocityLossToFriction(ball.getVelocityLossToFriction() * ball.getFrictionCoefficient());
            ball.setYVelocity(ball.getVelocityLossToFriction());
        } else {
            ball.setYVelocity(updatedVelocity);
        }
    }

    resetAnimationFrame(){
        if(!this.animate){
            this.animate = true;
            this.animateBalls();
        } else {
            this.animate = true;
        }
    }

    cancelAnimationFrame(){
        this.animate = false;
    }

    setColorPallet(colors){
        this.colorPallet = [...colors];
    }

    changeBallColors(){
        for(let ball of this.balls){
            ball.setColor(this.colorPallet[generateRandomNumber(0, this.colorPallet.length, true)]);
        }
    }

    addVelocityToSystem(min = this.yMinVelocity, max = this.yMinVelocity){
        for(let ball of this.balls){
            ball.setYVelocity(-generateRandomNumber(min, max));
            ball.setVelocityLossToFriction(ball.getYVelocity());
        }
    }
}