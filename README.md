# Bouncing Ball Animations for HTML5 Canvas
## Introduction
This project offers a few different animations which display in an HTML5 Canvas element. As of right now there are three different animations available:
 - Bouncing Balls - Simply a defined number of balls with randomly generated radii, colors (both stroke and fill), positions, and velocities. These balls will bounce off all 4 corners of the Canvas element.
 - Gravity Balls - Similar to Bouncing Balls, but each ball only moves in the vertical direction and will only 'bounce' off the bottom of the canvas element. A gravity is applied to each ball as well as a coefficient of friction to reduce the energy and mimic a real world bouncing ball.
 - Static Balls - Simply draws a defined number of circles, or balls, within the canvas element. This is still a work in progress to offer a better API and more options to edit each ball.
 Each type of animation has been separated out into its own module which can be included with a simple import statement at the top of your JS file.

 Some additional modules include Ball.js and UtilityTools.js.
 * Ball.js simply defines a ball and is used primarily for the above mentioned modules. Feel free to use this if you require a Ball object for anywhere else in your code.
 * UtilityTools.js are just some of the common functions which were being used. One generates a random number and one will generate a positive or negative sign value. These modules were not explicitly meant to be used, but can be (one of the examples below utilizes the random number generator). The API documentation will be included below if you feel like using them.

## Installing

Clone the project into your desired directory

```
git clone https://gitlab.com/Annarino/bouncingballs.git
```

Once cloned, ensure you include the JavaScript file properly into your HTML file. This project uses ES6 modules so your script tag should have a type of 'module'.
Example:
```
<script type="module" src="index.js"></script>
```

## Example
Clone the following project to see this library in use:
```
git clone https://gitlab.com/Annarino/bouncingballsexample.git
```

## API

### Bouncing Balls 
Include the BouncingBalls into the file:
```
import {BouncingBalls} from './BouncingBallsModules/bouncingBalls.js';
```
* Constructor - BouncingBalls(canvas, options)
    * canvas - [REQUIRED, CANVAS] A reference to the canvas element you wish to animate the balls in.
    * options - [OPTIONAL, OBJECT] An object which defines a set of options for the balls such as number of balls, maximum and minimum radius for each ball, etc.
        * Available options{}:
            * numberOfBalls - [INT] The amount of balls you would like to display on the screen. Defaults to 1.
            * maxRadius - [INT] The maximum radius for each ball. Defaults to 100.
            * minRadius - [INT] The minimum radius for each ball. NOTE - if you wish to have one radius, set the max and min radius size to the same value. Defaults to 1.
            * fillColorPallet - [ARRAY<STRING>] An array of color values to fill each ball with. Colors will be randomly assigned using this pallet. Defaults to ['rgba(0, 0, 255, 0.3)'].
            * strokeColorPallet - [ARRAY<STRING>] An array of color values for each ball's stroke. Colors will be randomly assigned using this pallet. Defaults to [ '#1f1f1f'].
            * xMaxVelocity - [INT] The maximum velocity in the x-plane (horizontal) a ball can have. Defaults to 10.
            * xMinVelocity - [INT] The minimum velocity in the x-plane (horizontal) a ball can have. Defaults to 5.
            * yMaxVelocity - [INT] The maximum velocity in the y-plane (vertical) a ball can have. Defaults to 10.
            * yMinVelocity - [INT] The minimum velocity in the y-plane (vertical) a ball can have. Defaults to 5.
* animateBalls() - Starts the animation. Must call this after instantiating the BouncingBalls

#### Example usage:

```
import {BouncingBalls} from './BouncingBallsModules/bouncingBalls.js';

const canvas = document.querySelector('canvas');
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
window.addEventListener('resize', () => {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
})

const options = {
    numberOfBalls: 100,
    maxRadius: 10,
    minRadius: 10,
    fillColorPallet: ['white'],
    strokeColorPallet: ['#155C11', '#35E82C', '#35E82C', '#27A820', '#1E8219'],
    yMaxVelocity: 2,
    yMinVelocity: 1,
    xMaxVelocity: 2,
    xMinVelocity: 1
}
const balls = new BouncingBalls(canvas, options);
balls.animateBalls();
```

### Gravity Balls 
Include the GravityBalls into the file:
```
import {GravityBalls} from './BouncingBallsModules/gravityBalls.js';
```
* Constructor - GravityBalls(canvas, options)
    * canvas - [REQUIRED, CANVAS] A reference to the canvas element you wish to animate the balls in.
    * options - [OPTIONAL, OBJECT] An object which defines a set of options for the balls such as number of balls, maximum and minimum radius for each ball, etc.
        * Available options{}:
            * numberOfBalls - [INT] The amount of balls you would like to display on the screen. Defaults to 1.
            * maxRadius - [INT] The maximum radius for each ball. Defaults to 100.
            * minRadius - [INT] The minimum radius for each ball. NOTE - if you wish to have one radius, set the max and min radius size to the same value. Defaults to 1.
            * fillColorPallet - [ARRAY<STRING>] An array of color values to fill each ball with. Colors will be randomly assigned using this pallet. Defaults to ['#41E8B0', '#64AC94', '#2C9C76', '#00E69A', '#1E6B51'].
            * strokeColorPallet - [ARRAY<STRING>] An array of color values for each ball's stroke. Colors will be randomly assigned using this pallet. Defaults to [ '#1f1f1f'].
            * yMaxVelocity - [INT] The maximum velocity in the y-plane (vertical) a ball can have. Defaults to 100.
            * yMinVelocity - [INT] The minimum velocity in the y-plane (vertical) a ball can have. Defaults to 50.
* animateBalls() - Starts the animation. Must call this after instantiating the GravityBalls
* setColorPallet(colorPallet) - Allows you to change the color pallet after instantiation. This will not automatically change the colors, you will need to call changeBallColors() after setting the new color pallet.
* changeBallColors() - Changes the color of each ball according to the updated color pallet.
* addVelocityToSystem(min, max) - Adds additional velocity in the vertical plane to the system. Will randomly calculate a number between the min and max. Set min and max equal to eachother if you wish to have each ball have the same amount of velocity applied.
    * min - [OPTIONAL, INT] The minimum amount of velocity to add to the system. Defaults to the value for yMinVelocity set in the constructor.
    * max - [OPTIONAL, INT] The maximum amount of velocity to add to the system. Defaults to the value set yMaxVelocity set in the constructor.
* resetAnimationFrame() - resets the animation frame. NOTE: this is only needed after the balls have stopped bouncing. For performance concerns, the animation frame quits when the balls have reached a minimum velocity which is less than the allowed minimum velocity of -1. You may not need to use this function, but if you notice your animation is not running then you should call resetAnimationFrame().

#### Example usage:

```
import {GravityBalls} from './BouncingBallsModules/gravityBalls.js';
import {generateRandomNumber} from './BouncingBallsModules/UtilityTools.js'

const canvas = document.querySelector('canvas');
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
window.addEventListener('resize', () => {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
})

const options = {
    numberOfBalls: 100,
    yMaxVelocity: 30,
    yMinVelocity: 20,
    maxRadius: 50,
    minRadius: 5,
    fillColorPallet: ['#5C0E06', '#E8240E', '#9C1809', '#A81A0A', '#851408']
}

const gravityBalls = new GravityBalls(canvas, options);
gravityBalls.animateBalls();

const colorPallets = [
    ['#155C11', '#35E82C', '#35E82C', '#27A820', '#1E8219'], 
    ['#41E8B0', '#64AC94', '#2C9C76', '#00E69A', '#1E6B51'],
    ['#5C0E06', '#E8240E', '#9C1809', '#A81A0A', '#851408']
];
window.addEventListener('mousedown', () => {
    gravityBalls.setColorPallet(colorPallets[generateRandomNumber(0, colorPallets.length, true)]);
    gravityBalls.changeBallColors();
    gravityBalls.addVelocityToSystem(10, 100);
    gravityBalls.resetAnimationFrame();
})
```

### Static Balls 
Include the StaticBalls into the file:
```
import { StaticBalls } from './BouncingBallsModules/staticBalls.js';
```
* Constructor - StaticBalls(canvas, options)
    * canvas - [REQUIRED, CANVAS] A reference to the canvas element you wish to animate the balls in.
    * options - [OPTIONAL, OBJECT] An object which defines a set of options for the balls such as number of balls, maximum and minimum radius for each ball, etc.
        * Available options{}:
            * numberOfBalls - [INT] The amount of balls you would like to display on the screen. Defaults to 1.
            * maxRadius - [INT] The maximum radius for each ball. Defaults to 100.
            * minRadius - [INT] The minimum radius for each ball. NOTE - if you wish to have one radius, set the max and min radius size to the same value. Defaults to 1.
            * fillColorPallet - [ARRAY<STRING>] An array of color values to fill each ball with. Colors will be randomly assigned using this pallet. Defaults to ['#41E8B0', '#64AC94', '#2C9C76', '#00E69A', '#1E6B51'].
            * strokeColorPallet - [ARRAY<STRING>] An array of color values for each ball's stroke. Colors will be randomly assigned using this pallet. Defaults to [ '#1f1f1f'].
* setColorPallet(colorPallet) - Allows you to change the color pallet after instantiation. This will not automatically change the colors, you will need to call changeBallColors() after setting the new color pallet.
* changeBallColors() - Changes the color of each ball according to the updated color pallet.
* resetBalls() - Will redraw the balls in different locations with different radii and colors according to the options passed into the constructor.

#### Example usage:
The following example will draw 150 balls. Whenever a user clicks, the color pallet of the balls will be randomly changed to one of the color pallets in 'colorPallets'.
```
import { StaticBalls } from './BouncingBallsModules/staticBalls.js';
import { generateRandomNumber, generateRandomSign } from './BouncingBallsModules/UtilityTools.js';

const canvas = document.querySelector('canvas');
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
window.addEventListener('resize', () => {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
})

const colorPallets = [
    ['#155C11', '#35E82C', '#35E82C', '#27A820', '#1E8219'], 
    ['#41E8B0', '#64AC94', '#2C9C76', '#00E69A', '#1E6B51'],
    ['#5C0E06', '#E8240E', '#9C1809', '#A81A0A', '#851408']
];

const options = {
    numberOfBalls: 150,
    minRadius: 10,
    maxRadius: 50
}
const balls = new StaticBalls(canvas, options);

window.addEventListener('click', () => {
    balls.setColorPallet(colorPallets[generateRandomNumber(0, colorPallets.length, true)]);
    balls.changeBallColors();
})
```

## Running the tests

Tests are in the making, check back soon...


## Author

* **Anthony Annarino** 


## Acknowledgments

* Chris Courses 'HTML5 Canvas Tutorials for Beginners | An intro to Becomming Pro' - https://www.youtube.com/watch?v=EO6OkltgudE&list=PLpPnRKq7eNW3We9VdCfx9fprhqXHwTPXL
